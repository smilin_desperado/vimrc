# smilin desperado's vimrc/init.vim
Instructions for my vim/neovim setup

# Requirements
- `git` (to install plugins from github or vim.org)

# Installation

## Neovim
*Mac*
- Use homebrew

```
brew install neovim
```

*Ubuntu*
- Use PPA

```
sudo add-apt-repository ppa:neovim-ppa/stable
sudo apt-get update
sudo apt-get install neovim
```

## vimrc/init.vim
*Vim 8+*

```
cat init.vim > ~/.vim/vimrc
```

*Neovim*

```
cat init.vim > ~/.config/nvim/init.vim
```

## Dein plugin manager

More info see https://github.com/Shougo/dein.vim/#unixlinux-or-mac-os-x

1. Run the script below:

```
curl https://raw.githubusercontent.com/Shougo/dein.vim/master/bin/installer.sh > installer.sh
sh ./installer.sh ~/.local/share/dein
```

## Install neovim plugins

1. Open nvim
2. Run function `:call dein#install()` to install all plugins in config file
3. Profit

