" Dein plugin manager ---------{{{
if &compatible
  set nocompatible
endif
" this is a test
" Add the dein installation directory into runtimepath
set runtimepath+=~/.local/share/dein/repos/github.com/Shougo/dein.vim

if dein#load_state('~/.local/share/dein')
  call dein#begin('~/.local/share/dein')

  call dein#add('~/.local/share/dein/repos/github.com/Shougo/dein.vim')

  " Custom plugins
  call dein#add('wsdjeg/dein-ui.vim')           "Dein ui - Package update
  call dein#add('tomasiser/vim-code-dark')      "Code dark (colour scheme)
  call dein#add('tpope/vim-commentary')         "vim commentary
  call dein#add('jiangmiao/auto-pairs')         "auto pairs
  call dein#add('w0rp/ale')                     "Ale (Asynchronous lint engine)
  call dein#add('posva/vim-vue')                "vue syntax highlighting
  call dein#add('leafgarland/typescript-vim')   "typescript syntax highlighting
  call dein#add('peitalin/vim-jsx-typescript')  "react typescript
  call dein#add('mileszs/ack.vim')              "search tool
  call dein#add('suan/vim-instant-markdown')    "markdown preview
  call dein#add('tpope/vim-fugitive')           "git wrapper
  call dein#add('vim-airline/vim-airline')
  call dein#add('vimwiki/vimwiki')              "vim wiki

  " End custom plugins

  call dein#end()
  call dein#save_state()
endif

let g:dein#install_log_filename = '~/.local/share/dein/install_log'
" }}}

" General settings ---------------- {{{
colorscheme codedark
syntax enable 		        "enable syntax processing
set tabstop=4 		        "number of visual spaces per tab
set softtabstop=4 	        "number of spaces in tab when editing
set expandtab		        "tabs are spaces
set number		            "show line numbers
set showcmd		            "show command in bottom bar
set cursorline		        "highlight current line
set autoindent              "auto indent
filetype indent on          "load filetype-specific indent files
set showmatch 		        "highlight matching [{()}]
set lazyredraw              "redraw only when we need to
set number relativenumber   "show hybrid numbers
set splitbelow              "new horizontal split appears below
set splitright              "new vertical split appears below
set autoread                "read files changed on disk

" Triger `autoread` when files changes on disk
" https://unix.stackexchange.com/questions/149209/refresh-changed-content-of-file-opened-in-vim/383044#383044
" https://vi.stackexchange.com/questions/13692/prevent-focusgained-autocmd-running-in-command-line-editing-mode
autocmd FocusGained,BufEnter,CursorHold,CursorHoldI * if mode() != 'c' | checktime | endif
" Notification after file change
" https://vi.stackexchange.com/questions/13091/autocmd-event-for-autoread
autocmd FileChangedShellPost *
  \ echohl WarningMsg | echo "File changed on disk. Buffer reloaded." | echohl None

set wildmenu        "visual autocomplete for command menu
set wildignorecase  "ignore case in wilemenu

let mapleader = " " "set leader to space

set shiftwidth=2    "Number of spaces to indent
set path+=**        "allow find to look for file recursively
set wildignore+=**/node_modules/**  "exclude node modules from file search
set ignorecase      "ignore case when searching
set incsearch       "search as characters are entered
set hlsearch        " highlight matches
set hidden          "allow switching buffers without forcing save
set clipboard+=unnamedplus  "always use clipboard for ALl operations
"set statusline=%F%m%r%h%w[%L][%{&ff}]%y[%p%%][%04l,%04v]
"              | | | | |  |   |      |  |     |    |
"              | | | | |  |   |      |  |     |    +-- current column
"              | | | | |  |   |      |  |     +-- current line
"              | | | | |  |   |      |  +-- current % into file
"              | | | | |  |   |      +-- current syntax
"              | | | | |  |   +-- current fileformat
"              | | | | |  +-- number of lines
"              | | | | +-- preview flag in square brackets
"              | | | +-- help flag in square brackets
"              | | +-- readonly flag in square brackets
"              | +-- rodified flag in square brackets
"              +-- full path to file in the buffer
" set statusline=%F[%04l,%04v]%{FugitiveStatusline()}
"
"set statusline=%n%<%F%m%r%{FugitiveStatusline()}\ %h%m%r%=%-14.(%l,%c%V%)\ %P
" Instant markdown
let g:instant_markdown_autostart = 0    "do not autostart preview
" set filetypes as typescript.tsx
autocmd BufNewFile,BufRead *.tsx,*.jsx set filetype=typescript.tsx
" }}}

" Key mappings ---------------------- {{{

"move vertically by visual line
nnoremap j gj
nnoremap k gk

"jk is escape
inoremap jk <esc>

" navigate between splits
nnoremap <silent> <leader>h :wincmd h<CR>
nnoremap <silent> <leader>j :wincmd j<CR>
nnoremap <silent> <leader>k :wincmd k<CR>
nnoremap <silent> <leader>l :wincmd l<CR>

" exit terminal mode
tnoremap <Esc> <C-\><C-n>

"Open vimrc in a split
nnoremap <leader>ev :vsplit $MYVIMRC<cr>

"Source vimrc
nnoremap <leader>sv :source $MYVIMRC<cr>

"highlight last inserted text
nnoremap gV `[v`]

"clear highlighted matches on space
nnoremap <silent> <space> :nohlsearch<CR><esc>

"swap colon/semicolon
nnoremap ; :
nnoremap : ;

"window resizing shortcuts
nnoremap <silent> <leader>wj :resize -10 <CR>
nnoremap <silent> <leader>wk :resize +10 <CR>
nnoremap <silent> <leader>wl :vertical resize +10 <CR>
nnoremap <silent> <leader>wh :vertical resize -10 <CR>

"unmap J - who needs to join lines!
nnoremap J <Nop>

"run search for string using ack"
nnoremap <leader>a :Ack! -i<Space>

" show markdown preview
nnoremap <silent> <leader>p :InstantMarkdownPreview <CR>

" show TODOs
nnoremap <leader>t :Ack! TODO<CR>

" git
nnoremap <silent> <leader>g :Gstatus<CR>
nnoremap <silent> <leader>gb :Gblame<CR>
nnoremap <silent> <leader>gd :Gdiffsplit!<CR>
" nnoremap <silent> <leader>gp :Gpush<CR>

" close current buffer
nnoremap <silent> <leader>dd :bp\|bd #<CR>

" }}}

" File browsing {{{

let g:netrw_banner = 0          "hide netrw banner
let g:netrw_browse_split = 0    "open files in current window
let g:netrw_winsize = 25        "window width

noremap <silent><leader>f :Explore<CR>
autocmd FileType netrw call s:netrw_settings()
function! s:netrw_settings()
  " Define mappings
  noremap <silent><buffer> <leader>f :q<CR>
  map <silent><buffer> n %<CR> "create file
  map <silent><buffer> N d<CR>  "new directory
endfunction

" }}}

" ALE {{{
" Enable completion where available.
" This setting must be set before ALE is loaded.
" let g:ale_completion_enabled = 1 
" let g:ale_open_list = 1             "show list of errors
let g:ale_list_window_size = 5      "change error window size
" let g:ale_keep_list_window_open = 1 "keep error window open
let g:ale_sign_column_always = 1       "keep indcator gutter open
let g:ale_echo_msg_format = '%linter%: %s'  "set error msg format
let g:ale_lint_delay = 300          "set delay before running linter
let g:ale_php_langserver_use_global = 1     "
let g:ale_php_langserver_executable = '/Users/pascal/.composer/vendor/felixfbecker/language-server/bin/php-language-server.php'
let g:ale_linters = {
  \ 'vue': ['vls', 'eslint'],
  \ 'php': ['langserver']
\ }
let g:ale_fixers = {
  \ 'javascript': ['prettier'],
  \ 'css': ['prettier'],
\ }
nnoremap gd :ALEGoToDefinition<CR>
nnoremap gr :ALEFindReferences<CR>
nmap <leader>d <Plug>(ale_hover)
nmap <leader>rr <Plug>(ale_lint)
"}}}

" Commentary {{{
"}}}


" Searching {{{
let g:ackprg = "rg --vimgrep --no-heading"
" }}}

" Vimwiki {{{
let g:vimwiki_list = [{'path': '~/Sync/vimwiki'}]
" }}}

set modelines=1
" Load all plugins now
packloadall
" Load all helptags now, after plugins have been loaded.
" All messages and errors will be ignored
silent! helptags ALL

" vim:foldmethod=marker:foldlevel=0
